package responshift

import (
	"encoding/json"

	"github.com/nicksnyder/go-i18n/v2/i18n"
	"gitlab.com/sugamaulana/go-responshift/constant"
	log "go.uber.org/zap"
)

// var langDir string = "constant/lang"

type Localizer struct{}

func (r *Responshift) InitLocalizer() error {
	r.bundle = i18n.NewBundle(r.Lang)
	r.bundle.RegisterUnmarshalFunc("json", json.Unmarshal)

	constant.InitMessage()
	for _, msg := range constant.ListMessage {
		r.bundle.AddMessages(msg.Lang, msg.Msg...)
	}

	return nil
}

func getLocalizedString(ctx Context, bundle *i18n.Bundle, messageID, module string) string {
	accept := ctx.GetHeader("Accept-Language")
	if bundle == nil {
		log.S().Infoln("Response localizer failed: No language loaded")
		return messageID
	}

	localizer := i18n.NewLocalizer(bundle, accept)

	msg, err := localizer.Localize(&i18n.LocalizeConfig{
		MessageID: messageID,
		TemplateData: map[string]interface{}{
			"module": module,
		},
	})
	if err != nil {
		return messageID
	}
	return msg
}
